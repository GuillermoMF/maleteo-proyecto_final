const express = require('express')
const User = require('../util/user')
const router = express.Router()

router.post('/register', (req, res, next) => {
    const recursos = {
        registrado: 'Usuario registrado',
        nRegistrado: 'Usuario ya registrado'
    }

    User.findOne({ email: req.body.email })
        .then(users => {
            if (users == null)
                guardar()
            else
                res.json({
                    registrado: true,
                    exito: false,
                    respuesta: recursos.nRegistrado
                })
        })

    const guardar = () => {

        console.log(req.body)
        datos = {
            email: req.body.email.toLowerCase(),
            name: req.body.name.toLowerCase(),
            surname: req.body.surname.toLowerCase(),
            pass: req.body.pass
        }

        const newUser = new User(datos)

        newUser.save()
            .then(respuesta => {
                console.log(respuesta)
                res.json({
                    registrado: true,
                    exito: true,
                    respuesta: recursos.registrado
                })
            })
            .catch(err => {
                console.log(err)
            })

    }

    console.log('estoy en register')
})

exports.router = router
// exports.schemas = schemas;
