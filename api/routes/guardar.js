const express = require("express");
const app = express();
const bodyParser = require ("body-parser");
const User = require("../util/user").User;

app.use("public", express.static("public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.get("/login", function(req,res){
    User.find(function(err,users){
        if (err) return console.error(err);
        res.render("/", {usuarios:users});
    });
});

app.post("/form", (req,res) => {
    const newUser = new User({
        email: req.body.email,
        name: req.body.name,
        surname: req.body.surname,
        pass: req.body.pass
    });


    newUser.save()
    .then(result => {
        res.redirect("/");
    })
    .catch(err => {
        console.log(err);
        res.send("No pudimos guardar la información");
    });
});
