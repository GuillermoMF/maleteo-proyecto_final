const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const PORT = 9000

const loginMaleteo = require('./routes/login')
const registerMaleteo = require('./routes/register')


const app = express()


app.use(express.json())
app.use(cors())

app.use('/', loginMaleteo.router)
app.use('/', registerMaleteo.router)

app.get('/', (req, res, next) => {
    res.send('Estas en API maleteo')
})


mongoose.connect(
    'mongodb://localhost/maleteo',
    { useNewUrlParser: true, useUnifiedTopology: true }
)
    .then(() => {
        app.listen(PORT, () => {
            console.log('El servidor Express esta activo.', PORT)
        })
    })
    .catch(error => {
        console.log(error)
    })
