
/**
 * Devuelve una lista de usuarios aleatorios
 * Recibe 3 parametros,  y Recursos
 * Recursos es un objeto con:
 *  Latitud,
 *  Longitud,
 *  minimo rango, 
 *  maximo rango, 
 *  decimales,
 *  Cantidad guardianes a devolver
 *  Guardian base
 */

const recursos = {
    rango: 5000, // metros
    cantidad: 10,
    coordenadas: {
        lat: 40.4170,
        lng: -3.7045
    }
}

const recursosBack = {
    decimales: 4,
    cantidad: 10,
    guardian: {
        id: 1,
        tipo: "particular",
        nombreApellido: "Ana Pérez Marín",
        coordenadas: {
            lat: 40.4170,
            lng: -3.7045
        },

        puntuacion: 5,
        icono: "/markers/home-marker.svg",
        iconoSelected: "/markers/home-marker-blue.svg",
        descripcion: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis, arcu vel ultricies luctus, odio ipsum iaculis orci, ac porttitor est lorem at leo. Nam odio purus, sagittis non sapien sit amet, vulputate lacinia felis. Duis elementum diam faucibus, consectetur turpis sit amet, lobortis risus. Vivamus malesuada imperdiet lectus ut pulvinar. Mauris at volutpat elit, nec lacinia est."
    }
}



const generarGuardianes = (recursos) => {
    const guardianes = []

    for (let i = 0; i < recursos.cantidad; i++) {
        guardianes.push(recursosBack.guardian)
        guardianes[i].coordenadas = generarGuardian(guardianes[i].coordenadas.lat, guardianes[i].coordenadas.lng, recursos.rango)
    }
    return guardianes
}

const generarGuardian = (nLat, nLng, rango) => {

    let nuevasCoordenadas = {
        lat: nLat,
        lng: nLng
    }

    nuevoGuardian.coordenadas.lat = random(nuevasCoordenadas.lat, rango.lat)
    nuevoGuardian.coordenadas.lng += random(nuevasCoordenadas.lng, rango.lng)

    console.log(nuevoGuardian.coordenadas)



    return nuevasCoordenadas
}

const random = (dis, rango) => {
    let ram = Math.random()
    let pow = Math.pow(100, recur.decimales)
    let floor = Math.floor(ram * ((num - recur.max) - (num - recur.min) + 1))
    let aux = floor + recur.min

    let res = aux / pow

    return res
}

console.log(aleatorio(recur))



