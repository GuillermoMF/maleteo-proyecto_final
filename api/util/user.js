const mongoose = require('mongoose');
const Schema = mongoose.Schema

mongoose.set('useFindAndModify', false);

const userSchema = new Schema({
    email: {
        //type: mongoose.Schema.Types.Email,
        type: String,
        require: true
    },
    name: {
        type: String,
        require: true
    },
    surname: {
        type: String,
        require: true
    },
    pass: {
        type: String,
        require: true
    }
});

module.exports = mongoose.model('User', userSchema);