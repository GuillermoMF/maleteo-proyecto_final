// const mongoose = require('mongoose');

// mongoose.connect('mongodb://localhost/auth:27017');

// module.exports = mongoose;


const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient

let _db
const mongoConnect = (callback) => {
  MongoClient.connect(
    'mongodb://localhost/maleteo:27017', { useNewUrlParser: true }
  )
    .then(client => {
      console.log('que pasa bro')
      _db = client.db()
      console.log('MongoDB Conectado')
      callback()
    })
    .catch(err =>{
      console.log(err)
      throw err;
    })
}
const getDb = () => {
  if(_db) {
    return _db
  }
  throw 'Base de datos no encontrada'
}
exports.mongoConnect = mongoConnect
exports.getDb = getDb
