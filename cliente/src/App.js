import React, { useContext } from 'react'
import { Switch, Route } from 'react-router-dom'
import { Direcciones } from './util/Recursos'
import './App.css';
import './styles/styles.css'
import Conexion from './componentes/Conexion/Conexion'
import Inicio from './componentes/Inicio/Inicio'
import Tarifas from './componentes/Tarifas/Tarifas'
import PaginaHome from './componentes/PaginaHome/PaginaHome'
import EnConstruccion from './componentes/EnConstruccion/EnConstruccion'



const App = () => {
  const routerDir = useContext(Direcciones)

  return (
    <div className="text">
      <Switch>
        <Route path={routerDir.inicio} exact component={Inicio} />
        <Route path={routerDir.inicio + routerDir.login} exact component={Conexion} />
        <Route path={routerDir.inicio + routerDir.tarifas} exact component={Tarifas} />
        <Route path={routerDir.inicio + routerDir.home} exact component={PaginaHome} />
        <Route path={routerDir.inicio + routerDir.home + "/" + routerDir.buscar} exact component={EnConstruccion} />
        <Route path={routerDir.inicio + routerDir.chat} exact component={EnConstruccion} />
        <Route path={routerDir.inicio + routerDir.perfil} exact component={EnConstruccion} />
      </Switch>
    </div>
  )
}

export default App;
