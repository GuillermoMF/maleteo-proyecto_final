import React, {Component} from 'react'
import zaragoza from './assets/images/zaragoza.jpg'
import Estrellitas from '../Estrellitas/Estrellitas.js'

const foto={
    width:'100%',
    borderRadius:'4px'
};
const myStyle = {
    color:'gray',
}

class CardZaragoza extends Component{
render(){
    return(

        <div style={myStyle}>
            <img src={zaragoza} style={foto} alt="zaragoza"/>
            <p>
                <strong>
                    La ciudad bajo la Pilarica
                </strong>
            </p>
            <p>
                Historia viva, monumentos. Una ciudad viva en la que encontrar
                todo lo que quieras, tanto cultura como gastronomía.
            </p>
            <Estrellitas></Estrellitas>
        </div>
        );
    };

}

export default CardZaragoza;
