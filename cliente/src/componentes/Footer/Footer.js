import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { Direcciones } from '../../util/Recursos'
import casa from './assets/images/house.svg';
import lupita from './assets/images/lupita.svg';
import comentame from './assets/images/comments.svg';
import persona from './assets/images/man.svg';

const FOO = {
    display: 'flex',
    justifyContent: 'space-around',
    borderTop: 'solid grey 1px',
    paddingTop: '15px',
    paddingBottom: '15px',

}

const wrap = {
    width: '100%',
    background: '#ffffff',
    position: 'fixed',
    bottom: '0'
}
const hg = {
    height: '30px'
}
const Footer = () => {
    const routerDir = useContext(Direcciones)
    return (
        <div style={wrap}>
            <div style={FOO}>
                <Link to={routerDir.inicio + routerDir.home}>
                    <img src={casa} alt="casa" style={hg} />
                </Link>
                <Link to={routerDir.inicio + routerDir.home + "/" + routerDir.buscar}>
                    <img src={lupita} alt="lupita" style={hg} />
                </Link>
                <Link to={routerDir.inicio + routerDir.chat}>
                    <img src={comentame} alt="comentame" style={hg} />
                </Link>
                <Link to={routerDir.inicio + routerDir.perfil}>
                    <img src={persona} alt="persona" style={hg} />
                </Link>
            </div>

        </div >
    );
}

export default Footer;