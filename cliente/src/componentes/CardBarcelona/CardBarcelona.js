import React, {Component} from 'react';
import barcelona from './assets/images/barcelona.jpg';
import Estrellitas from '../Estrellitas/Estrellitas.js'

const foto={
    width:'100%',
    borderRadius:'4px'
}
const myStyle = {
    color:'gray',
}


class CardBarcelona extends Component{
    render(){
        return(

            <div style={myStyle}>
                <img src={barcelona} style={foto} alt="barcelona"/>
                <p>
                    <strong>
                        La ciudad condal
                    </strong>
                </p>
                <p>
                    La ciudad Cosmopolitan por excelencia, 
                    respeto, cultura e historia de 
                    España mires por donde mires.
                </p>
                <Estrellitas></Estrellitas>
            </div>
            );
        };

}

export default CardBarcelona;
