import React from 'react';
import EstrellitasCards from '../EstrellitasCards/EstrellitasCards';

const section = {
    backgroundColor: '#fff',
    width: '86%',
    borderRadius: '7px',
    boxShadow: '0px 0px 7px #aaa',
    zIndex: '10',
    padding: '5%',
    position: 'fixed',
    bottom: '2%',
    marginLeft: '2%',
    paddingBottom: '5%'
    //marginRight: '2%'
}

const imagen = {
    width: '100%',
    height: '100%',
    borderRadius: '7px 0px 0px 7px'
}

const articleImagen = {
    width: '52%'
}

const cuadroGrande = {
    //backgroundColor: 'red',
    borderRadius: '7px',
    display: 'flex',
    boxShadow: '1px 1px 3px #aaa',
    width: '100%',
    height: '100%'
}

const articleDescripcion = {
    backgroundColor: '#fff',
    marginLeft: '3%',
    marginRight: '3%',
    width: '48%'
}

const imagenRedondeada = {
    borderRadius: '10%',
    marginTop: '0.5em',
    boxShadow: '1px 1px 3px #aaa'
}

const h1Style= {
    width: '100%',
    padding: '0',
    margin: '0',
    marginTop: '0.5em'
}

const estrellitas = {
    marginTop: '0.5em'
}

const descripcionP = {    
    position: 'relative',
    display: 'block', /* or inline-block */
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    wordWrap: 'break-word',
    maxHeight: '2.4em',
    lineHeight: '1.2em',
    margin: '0px'
}
const puntosSuspensivos = {
    marginTop: '0.5em',
}

const puntosP = {
    marginTop: '0.1em',
    color: '#444'
}

const GuardianPopUp = ({guardian}) => {

    return (
        <section style={section}>
            <div style={cuadroGrande}>

                <article style={articleImagen} className="imagen">
                    <img style={imagen} src="https://via.placeholder.com/1200" alt="Local_Placeholder"></img>
                </article>

                <article style={articleDescripcion} className="descripcion">
                    <h1 style={h1Style} className="nombreGuardian">{guardian.nombreApellido}</h1>

                    <div>
                        <img style={imagenRedondeada} className="fotoFuardian" src="https://via.placeholder.com/75" alt="Guardian_Placeholder"></img>
                    </div>
                    
                    <div style={estrellitas} className="rating">
                        <EstrellitasCards guardian={guardian} />
                    </div>

                    <div style={puntosSuspensivos}>
                        <p style={descripcionP}> {guardian.descripcion} </p>
                        <p style={puntosP}> ... </p>
                    </div>

                </article>

            </div>
        </section>
    )}

export default GuardianPopUp