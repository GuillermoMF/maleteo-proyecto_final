import React , { Component } from 'react';
import Calendar from 'react-calendar';

const Calendario ={ 
    marginTop:'14%',
    paddingLeft: '4%',
    width: '89%',
}
 
class BigCalendar extends Component { 
    state  = { 
      date:new Date (),  
    }
    onChange = date =>this.setState({date})
    render (){ 
      return( 
        <div style={Calendario}>
          <Calendar
            onChange = {this.onChange}
            value = {this.state.date}
          />
        </div>
      
      );
    }
  }
export default BigCalendar;