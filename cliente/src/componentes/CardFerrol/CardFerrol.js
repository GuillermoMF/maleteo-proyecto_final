import React, { Component } from 'react';
import ferrol from './assets/images/ferrol.jpg';
import Estrellitas from '../Estrellitas/Estrellitas.js'

const foto = {
    width: '100%',
    borderRadius: '4px'
}
const myStyle = {
    color: 'gray',
}


class CardFerrol extends Component {
    render() {
        return (

            <div style={myStyle}>
                <img src={ferrol} style={foto} alt="ferrol" />
                <p>
                    <strong>
                        Donde se acaba el mar
                </strong>
                </p>
                <p>
                    La ciudad donde se acaba el mar, unas vistas impresionantes,
                    una gastronomía fresca de las de verdad.
            </p>
                <Estrellitas></Estrellitas>
            </div>
        );
    };

}

export default CardFerrol;
