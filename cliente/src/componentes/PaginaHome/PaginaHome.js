import React from 'react'
import HeaderHome from '../HeaderHome/HeaderHome'
import FormHome from '../FormHome/FormHome'
import Novedades from '../Novedades/Novedades'
import Experiencias from '../Experiencias/Experiencias'
import Footer from '../Footer/Footer'

const pat = {
    paddingBottom: '30px'
}
const pot = {
    paddingBottom: '20px'
}

const PaginaHome = () => {

    return (
        <div style={pat}>
            <div style={pot}>
                <HeaderHome></HeaderHome>
                <FormHome></FormHome>
                <Novedades></Novedades>
                <Experiencias></Experiencias>
            </div>
            <Footer></Footer>
        </div>
    )
}

export default PaginaHome