import React, { Component } from 'react';
import Flecha from './assets/images/right-arrow.png'

const Boton ={
    borderRadius:'50%',
    background:'#FF8300',
    marginTop:'26%',
    marginLeft: 'auto',
    width:'30px',
    height:'30px',
    

}
const Flecha1 ={
    backgroundImage:`url(${Flecha})`,
    height:'100%',
    backgroundSize:'cover',
    height: '80%',
    margin: '0 11% 0 -5%',
}
const Boton1 = {
    paddingRight:'10%',
}

class BotonSiguiente extends Component{
    render(){
      return( 
         <div style={Boton1}>
          <div style={Boton}>
             <div style={Flecha1}></div>
          </div>
        </div> 
        
        );
      }
}

export default BotonSiguiente;
