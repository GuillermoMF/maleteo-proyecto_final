import React, { Fragment, useState } from 'react'
import Portada from './assets/componentes/Portada/Portada'
import Bienvenida1 from './assets/componentes/Bienvenida1/Bienvenida1'
import Bienvenida2 from './assets/componentes/Bienvenida2/Bienvenida2'


const Inicio = () => {
    const [visible, setVisible] = useState(0)

    const cambio = () => {
        setVisible(visible + 1)
    }

    return <Fragment>
        {visible === 0 && <Portada cambio={cambio} />}
        {visible === 1 && <Bienvenida1 cambio={cambio} />}
        {visible === 2 && <Bienvenida2 />}
    </Fragment>
}
export default Inicio