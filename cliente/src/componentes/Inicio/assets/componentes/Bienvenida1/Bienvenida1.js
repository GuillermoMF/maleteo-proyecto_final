import React from 'react';
import chain from './assets/images/chain.svg';

const myStyle1 = {
    textAlign: 'center',
    color: 'black',
    border: '1px solid white',
    background: 'linear-gradient(180deg, #fecd3d 0%, #fab13b 100%)',
    minHeight: '50em',
    overflow: 'hidden',
    padding: '2%',
    margin: '-1px'
}
const myStyle2 = {
    width: '30%',
    padding: '10%'
}
const myStyle3 = {
    borderRadius: '4px',
    boxShadow: '1px 2px 7px grey',
    paddingBottom: '10%',
    background: 'white',
    fontfamily: 'sans-serif',
    color: '#55565A',
}
const myStyle4 = {
    color: 'white',
    background: '#FF8300',
    width: '70%',
    margin: 'auto',
    borderRadius: '4px',
    fontWeigth: 'bold',
    padding: '2%',
}

const aStyle = {
    textDecoration: 'none',
}
const pStyle = {
    padding: '5%',
}

const Bienvenida = (props) => {
    return (
        <div style={myStyle1}>
            <div style={myStyle3}>
                <img src={chain} style={myStyle2} alt="chain"></img>
                <p style={pStyle}>Encuentra a tu guardián y disfruta a tu manera. Miles de usuarios ya están aprovechando sus ventajas</p>
                <div style={aStyle}>
                    <div style={myStyle4} onClick={props.cambio}>
                        Empezar ya
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Bienvenida;

