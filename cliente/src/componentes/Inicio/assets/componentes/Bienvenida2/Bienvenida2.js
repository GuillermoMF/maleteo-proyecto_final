import React, { useContext } from 'react';
import { Link } from 'react-router-dom'
import { Direcciones } from '../../../../../util/Recursos'
import earth from './assets/images/earth1.svg'

const myStyle1 = {
    textAlign: 'center',
    color: 'black',
    border: '1px solid white',
    background: 'linear-gradient(180deg, #fecd3d 0%, #fab13b 100%)',
    minHeight: '50em',
    overflow: 'hidden',
    padding: '2%',
    margin: '-1px'
};
const myStyle2 = {
    width: '30%',
    padding: '10%'
}
const myStyle3 = {
    borderRadius: '4px',
    boxShadow: '1px 2px 7px grey',
    paddingBottom: '10%',
    background: 'white',
    fontfamily: 'sans-serif',
    color: '#55565A',
}
const myStyle4 = {
    color: 'black',
}
const myStyle5 = {
    paddingTop: '5%',
}
const myStyle6 = {
    color: 'white',
    background: '#FF8300',
    width: '70%',
    margin: 'auto',
    borderRadius: '4px',
    fontWeigth: 'bold',
    padding: '2%',
}

const aStyle = {
    color: 'black',
    fontSize: '14px',
}
const aStyle2 = {
    textDecoration: 'none',
}
const pStyle = {
    padding: '5%',
}
const Bienvenida2 = () => {
    const routerDir = useContext(Direcciones)

    const cambioUltima = () => {
        routerDir.ultima = routerDir.inicio
    }

    return (
        <div style={myStyle1}>
            {cambioUltima()}
            <div style={myStyle3}>
                <img src={earth} style={myStyle2} alt="logo"></img>
                <h1 style={myStyle4}>El mismo precio en cualquier parte</h1>
                <p style={pStyle}>Dispondrás de un precio fijo estés donde estés sin importar el tamaño o el peso</p>
                <div style={aStyle2}>
                    <Link to={routerDir.inicio + routerDir.login} style={myStyle6} >Continuar</Link>
                </div>
                <div style={myStyle5}>
                    <Link to={routerDir.inicio + routerDir.tarifas} style={aStyle}>Consulta los precios</Link>
                </div>
            </div>
        </div>
    );
}
export default Bienvenida2;

