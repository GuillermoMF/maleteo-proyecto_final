import React from 'react';
import logo from './assets/images/logo.svg';

const myStyle1 = {
    color: 'black',
    border: '1px solid white',
    background: 'linear-gradient(180deg, #fecd3d 0%, #fab13b 100%)',
    minHeight: '50em',
    overflow: 'hidden',
    padding: '2%',
    margin: '-1px'
};
const myStyle2 = {
    width: '100%',
    padding: '50vh 0% 0% 0%'
}
const Portada = (props) => {
    return (
        <div style={myStyle1} onClick={props.cambio}>
            <img src={logo} style={myStyle2}alt="logo"></img>
        </div>
    );
}
export default Portada;

