import React from 'react';
import { Slide } from 'react-slideshow-image';
import montania from './assets/images/montania.jpg';
import madrid from './assets/images/madrid.jpg';
import guardianes from './assets/images/guardianes.jpg';
import CardBlog from '../CardBlog/CardBlog.js';
import CardCiudades from '../CardCiudades/CardCiudades.js';
import CardGuardian from '../CardGuardian/CardGuardian.js';


const slideImages = [
  montania,
  madrid,
  guardianes
];


const properties = {
  duration: 500,
  transitionDuration: 3000,
  infinite: true,
}
const noneA = {
  backgroundImage: `url(${slideImages[0]})`,
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  borderRadius: '4px',
  marginRight: '20px',
  height: '100%'

}
const noneB = {
  backgroundImage: `url(${slideImages[1]})`,
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  borderRadius: '4px',
  marginRight: '20px',
  height: '100%'



}
const noneC = {
  backgroundImage: `url(${slideImages[2]})`,
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  borderRadius: '4px',
  marginRight: '20px',
  height: '100%',
}
const myStyle = {
  backgroundColor: 'rgba(45, 212, 196, 0.3)',
  height: '100%'
}
const Slideshow = () => {



  return (
    <div className="slide-container" >
      <Slide {...properties}>
        <div className="each-slide" style={noneA}>
          <div style={myStyle}>
            <CardBlog></CardBlog>
          </div>
        </div>
        <div className="each-slide" style={noneB}>
          <div style={myStyle}>
            <CardGuardian></CardGuardian>
          </div>
        </div>
        <div className="each-slide" style={noneC}>
          <div style={myStyle}>
            <CardCiudades></CardCiudades>
          </div>
        </div>
      </Slide>
    </div>
  )
}

export default Slideshow;