import React, { Fragment, useState, useContext } from 'react'
import { Redirect } from 'react-router-dom'
import { Direcciones } from '../../util/Recursos'
import Atras from '../Atras/Atras'
import './assets/styles/Tarifas.css'

const myStyle1 = {
    textAlign: 'center',
    color: 'black',
    border: '1px solid white',
    background: 'white',
    minHeight: '50em',
    margin: '-1px'
};

const myStyle2 = {
    width: '90%',
    background: 'white',
    color: 'gray',
    padding: '2%',
    borderRadius: '4px',
}
const myStyle3 = {
    borderRadius: '4px',
    boxShadow: '1px 1px 2px 2px #88898',
    paddingBottom: '10%',
    background: 'white',
    fontfamily: 'sans-serif',
    color: 'black',
}
const myStyle4 = {
    borderRadius: '4px',
    color: 'white',
    fontSize: '32px',
    width: '80%',
    background: '#FF8300' //orange
}
const myStyle5 = {
    borderRadius: '4px',
    color: 'white',
    fontSize: '32px',
    width: '80%',
    background: '#17AEE2' //blue
}
const pStyle = {
    fontSize: '16px',
    paddingTop: '4%',
}
const pStyle2 = {
    margin: '0px'
}
const pStyle3 = {
    fontSize: '16px',
    paddingBottom: '4%',
}
const speechBubble = {

    position: 'absolute',
    content: '',
    bottom: '0',
    left: '87%',
    width: '0',
    height: '0',
    border: '0.588em solid transparent',
    borderTopColor: '#fc843a',
    borderBottom: '0',
    borderRight: '0',
    marginLeft: '-0.344em',
    marginBottom: '0.73em',

}
const speechBubble2 = {

    position: 'absolute',
    content: '',
    bottom: '0',
    left: '89%',
    width: '0',
    height: '0',
    border: ' 0.588em solid transparent',
    borderTopColor: '#fc843a',
    borderBottom: '0',
    borderLeft: '0',
    marginLeft: '-0.344em',
    marginBottom: '0.73em',

}
const speechBubble3 = {

    position: 'absolute',
    content: '',
    bottom: '0',
    left: '87%',
    width: '0',
    height: '0',
    border: '0.588em solid transparent',
    borderTopColor: 'white',
    borderBottom: '0',
    borderRight: '0',
    marginLeft: '-0.344em',
    marginBottom: '0.93em',

}
const speechBubble4 = {

    position: 'absolute',
    content: '',
    bottom: '0',
    left: '89%',
    width: '0',
    height: '0',
    border: ' 0.588em solid transparent',
    borderTopColor: 'white',
    borderBottom: '0',
    borderLeft: '0',
    marginLeft: '-0.344em',
    marginBottom: '0.93em',

}
const myStyle6 = {
    paddingRight: '0px',
    display: 'flex',
    position: 'relative',
}
const Tarifas = () => {
    const routerDir = useContext(Direcciones)
    const [rediret, setRedirect] = useState(false)

    const redireccionar = () => {
        if (rediret)
            return <Redirect to={routerDir.inicio} />
    }

    return (
        <Fragment>
            {redireccionar()}
            <div className="container-fluid">
                <div style={myStyle1}>
                    <div className="row">
                        <div className="btnAtras">
                            <Atras volver={() => setRedirect(true)} />
                        </div>
                    </div>
                    <div className="row">
                        <div style={myStyle3}>
                            <h1>Selecciona</h1>
                            <form>
                                <label>
                                    <div style={myStyle6}>
                                        <select style={myStyle2}>
                                            <option>Europa</option>
                                            <option>Asia</option>
                                            <option>America Norte</option>
                                            <option>America Sur</option>
                                            <option>Oceanía</option>
                                            <option>África</option>
                                        </select>
                                        <div style={speechBubble}></div>
                                        <div style={speechBubble2}></div>
                                        <div style={speechBubble3}></div>
                                        <div style={speechBubble4}></div>

                                    </div>

                                </label>
                            </form>
                            <h1>Nuestras Tarifas Fijas</h1>
                            <div style={myStyle4}>
                                <p style={pStyle}>24 Horas</p>
                                <p style={pStyle2}>6€</p>
                                <p style={pStyle3}> Por equipaje</p>
                            </div>
                            <div style={myStyle5}>
                                <p style={pStyle}>Día Adicional</p>
                                <p style={pStyle2}>4€</p>
                                <p style={pStyle3}> Por equipaje</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
export default Tarifas;

