import React, { Component } from 'react';
import Slideshow from '../Slideshow/Slideshow';
const nov = {
    margin: '2em',

}

class Novedades extends Component {
    render() {
        return (
            <div style={nov}>
                <div>
                    <h1>
                        <strong>
                            Novedades
                    </strong>
                    </h1>
                </div>
                <div>
                    <Slideshow></Slideshow>
                </div>

            </div>
        );
    };
}

export default Novedades;
