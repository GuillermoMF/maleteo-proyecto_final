import React, { Component } from 'react';
import linea from './assets/images/linea.svg';
import Blog from './assets/images/book.svg'
const CardBlogStyle = {
    width: '75%',
}
const svg = {
    height: '15%',
    width: '29%',
    marginTop: 'auto',
    marginBottom: 'auto',
}
const svgblog = {
    display: 'flex'
}
const line = {
    maxHeight: '2px',
    width: '100%'
}
const color = {
    color: 'white'
}
const Bp = {
    marginLeft: '30px'
}




class CardBlog extends Component {
    render() {
        return (
            <div>
                <section style={CardBlogStyle}>
                    <div className="svgblog" style={svgblog}>
                        <img className="Blog" src={Blog} alt="Blog" style={svg} />
                        <div className="write" style={Bp}>
                            <h1 style={color}>BLOG</h1>
                            <p style={color}>Un nuevo artículo de nuestro viajero empedernido</p>
                        </div>
                    </div>
                    <div className="linea">
                        <img src={linea} alt="linea" style={line} />
                    </div>
                    <div className="Leermas">
                        <p style={color}>Leer más</p>
                    </div>
                </section>
            </div>
        );
    };
}

export default CardBlog;