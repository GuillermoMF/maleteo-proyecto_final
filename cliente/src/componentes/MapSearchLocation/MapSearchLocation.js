import React, { Component } from 'react';
import {GoogleComponent} from 'react-google-location';

class HomeComponent extends Component {
    constructor(props) {
      super(props)
      this.state = {
        place: null,
      };
    }
   
    render() {
      console.warn("result return here" , this.state.place)
      return (
        <div >
           <GoogleComponent
           
            apiKey={"AIzaSyCMq55J4NHJ1dzOtMOgYKVgYWN3g4WD4A"}
            language={'es'}
            country={'country:in|country:us'}
            coordinates={true}
            //locationBoxStyle={'custom-style'}
            //locationListStyle={'custom-style-list'}
            onChange={(e) => { this.setState({ place: e }) }} 
            />
        </div>
   
      )
    } 
  }
   
   
  export default HomeComponent;