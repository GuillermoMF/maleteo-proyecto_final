import React, { Component } from 'react';
import Barcelona from '../CardBarcelona/CardBarcelona';
import Ferrol from '../CardFerrol/CardFerrol';
import Sevilla from '../CardSevilla/CardSevilla';
import Zaragoza from '../CardZaragoza/CardZaragoza';


let Cities = [
    <Barcelona></Barcelona>,
    <Sevilla></Sevilla>,
    <Zaragoza></Zaragoza>,
    <Ferrol></Ferrol>
];
const myStyle = {
    textAlign: 'center',
    width: '80%',
    border: '#FF8300 1px solid',
    background: 'transparent',
    padding: '5%',
    color: '#FF8300',
    fontWeight: 'bold',
    borderRadius: '4px'
}
const myStyle2 = {
    textAlign: 'center'
}
class Boton extends Component {
    constructor(props) {
        super(props);
        this.toca = this.toca.bind(this);
        this.state = { items: [] }
    }

    toca(e) {
        let newArray = this.state.items;
        let l = newArray.length;
        newArray.push(Cities[l])
        this.setState({
            items: newArray
        });
    }
    render() {
        return (
            <div>
                <div>
                    {this.state.items}
                </div>
                <div style={myStyle2}>
                    {!(Cities.length === this.state.items.length) && <button style={myStyle} onClick={this.toca}>Mostrar más</button>}
                </div>
            </div>
        );
    }

}


export default Boton;
