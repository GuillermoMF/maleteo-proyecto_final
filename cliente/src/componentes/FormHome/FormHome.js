import React, { Component } from 'react';
import Nube from '../Popup/Popup'
import Cal from '../PopupCalendario/PopupCalendario.js'

const formulario = {
    margin: '2em'
};
const nube = {
    maxWidth: '100%',
    overFlow: 'hidden',
    position: 'absolute',
    top: '0px',
    left: '0px'

}

class FormHome extends Component {

    //Constantes varias de posición etc
    render() {
        return (
            <div className="formulario" style={formulario}>
                <Nube style={nube}></Nube>
                <Cal></Cal>
            </div>
        );
    };
}


export default FormHome;