import React from 'react';

import BigCalendar from '../BigCalendar/BigCalendar';
import BotonContinuar from '../BotonContinuar/BotonContinuar';
import Icono from '../Icono/Icono.js';
import HorayEquipaje from '../HoraYEquipaje/HoraYEquipaje';


function AppCalendario(props) {
  return (
    <div>
      <HorayEquipaje setStateEquipaje={props.setStateEquipaje}></HorayEquipaje>
      <BigCalendar></BigCalendar>
      <BotonContinuar></BotonContinuar>
      <Icono></Icono>
    </div>
  );
}
export default AppCalendario 