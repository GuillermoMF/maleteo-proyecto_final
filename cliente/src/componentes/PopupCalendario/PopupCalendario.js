import React, { Component } from 'react';
import Popup from "reactjs-popup";
import Calendario from "../AppCalendario/AppCalendario"
import calendar from './assets/images/calendar.svg'
import maleta from './assets/images/bagage.svg'


const depos = {
    width: '48%',
    marginRight:'4%',
};

const ret = {
    width: '48%'
};

const calendarioDeposito = {
    display: 'flex',
    width: '100%',
    Height: '38px',
    background:'transparent',
    border:'none',
    boxShadow:'1px 1px 3px gray'
};
const calendarioRetirado = {
    display: 'flex',
    width: '100%',
    maxHeight: '38px',
    background:'transparent',
    border:'none',
    boxShadow:'1px 1px 3px gray'
};
const p = {
    alignItems: 'center'
};

const DR = {
    display: 'flex',
    maxHeight: '38px',
    marginTop:'4px'
    
};

const cal = {
    width: '48%',
    alignItems: 'center'

};
const deposito = {
    height: '38px',
    display: 'flex',
    alignItems: 'center',
    marginTop: '-5px'

};
const retirada = {
    height: '38px',
    display: 'flex',
    alignItems: 'center',
    marginTop: '-5px'
}
const calendarD = {
    display: 'flex',
    alignItems: 'center'


}

const malt = {
    width: '45%',
    display: 'flex',
    justifyContent: 'flex-start'

};

const numpiezas = {
    marginTop:'4px',
    display: 'flex',
    width: '100%',
    height: '38px'
};
const botin = {
    display: 'flex',
    height: '38px',
    alignItems: 'center',
    background:'transparent',
    border:'none',
    boxShadow:'1px 1px 3px gray'
};
const buscarback = {
    backgroundColor: '#FF8300',
    width: '100%',
    height: '38px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '8%',
    border: 'none',
    borderRadius:'4px'
};
const b = {
    color: 'white'
}
const npizd = {
    width: '48%'
}
const npizq = {
    width: '48%',
    marginRight:'4%'
};
const piezas = {
    paddingRight: '29px',
    paddingTop: '3px'
}







class PopupCalendario extends Component {
    constructor(props) {
      super(props);
      this.state = { open: false };
      this.openModal = this.openModal.bind(this);
      this.closeModal = this.closeModal.bind(this);
      
    }
    openModal() {
      this.setState({ open: true });
    }
    closeModal() {
      this.setState({ open: false });
    }
   
  
    render() {
      return (
    <div>    
        <div>
                    <div style ={DR}>
                            <div className ="depos" style ={depos}>
                                        <button className ="calendarioDeposito" style ={calendarioDeposito} onClick={this.openModal}>
                                            <div className ="calendarD"  style ={calendarD}>
                                                <img className ="cldD" src ={calendar} alt ="calendario" style ={cal}/>
                                            </div>
                                            <div className ="deposito" style ={deposito}>
                                                <p style ={p}>
                                                    Depósito 
                                                </p>
                                            </div>
                                        </button> 
                                    </div> 
                                    <div className ="ret" style ={ret}>
                                        <button className ="calendarioRetirado" style ={calendarioRetirado} onClick={this.openModal}>
                                            <div className ="calendarD" style ={calendarD}>
                                                <img className ="cldR" src ={calendar} alt ="calendario" style ={cal}/> 
                                            </div>
                                            <div className ="retirada" style ={retirada}>
                                                <p style ={p}>
                                                    Retirada 
                                                </p>
                                            </div>
                                        </button>
                                    </div>
                </div>
    
        
    
                    <div className ="PBusq" style ={numpiezas}>
                                        <div style ={npizq}>
                                            <button className ="npiezas" style ={botin} onClick={this.openModal}>
                                                <div>
                                                    <img className ="maleta" src ={maleta} alt ="maleta" style ={malt}/> 
                                                </div>
                                                <div className ="piezas" style ={piezas}>
                                                    <p style ={p}>
                                                        N.Piezas
                                                        </p>
                                                </div>
                                            </button> 
                                        </div> 
                                        <div style ={npizd}>
                                            <button className ="buscarback" style ={buscarback}>
                                                <div>
                                                    <p style ={b}>
                                                        Buscar 
                                                    </p> 
                                                </div>
                                            </button> 
                                        </div> 
                                        </div>     
                </div>
    
          <Popup
            open={this.state.open}
            closeOnDocumentClick
            onClose={this.closeModal}
          >
            <div className="modal">
              <a className="close" onClick={this.closeModal}>
                &times;
              </a>
              <Calendario></Calendario>
            </div>
          </Popup>
        </div>
  
      );
    }
  }
  
  export default PopupCalendario ;