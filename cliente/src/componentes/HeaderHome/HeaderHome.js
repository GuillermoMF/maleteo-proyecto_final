import React, { Component } from 'react';
import './assets/styles/headerHome.css';

const header = {
    margin: '2em',
    marginBottom: '-1em'
};
class HeaderHome extends Component {
    render() {
        return (
            <div className="header-home" style={header}>
                <h1>
                    <strong>
                        Encuentra tu guardián
                </strong>
                </h1>
            </div>
        );
    }
}

export default HeaderHome;
