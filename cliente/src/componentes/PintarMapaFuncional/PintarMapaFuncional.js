import React, { useState, useEffect } from "react";
import {
    GoogleMap,
    withScriptjs,
    withGoogleMap,
    Marker,
    InfoWindow
} from "react-google-maps";
import GuardianPopUp from "../GuardianPopUp/GuardianPopUp"
import mapStyles from '../../util/mapStyles'
import guardianes from '../../util/guardianes'
import home_marker from './assets/images/home-marker.svg'
import map_marker_green from './assets/images/map-marker-green.svg'
import shop_marker from './assets/images/shop-marker.svg'

const PintarMapaFuncional = ({ newCenter }) => {
    console.log("El location de PintarMapaFuncional es: ", newCenter);
    //console.log("La posición geolocalizada en PintarMapaFuncional es: ", geolocation)

    const [center, setCenter] = useState({
        lat: 40.458631,
        lng: -3.694984
    });
    //const [icon, setIcon] = useState("");
    const [selectedIcon, setSelectedIcon] = useState(null);

    const handleClick = guardian => {
        setSelectedIcon(guardian);
        //if (guardian.tipo==="particular"){
        //
        //}
    };

    console.log("El centro viejo es: ", center);

    //useEffect(()=>{
    //    setCenter(newCenter)
    //},[newCenter])

    console.log("El nuevo centro es: ", center);

    return (
        <GoogleMap
            defaultZoom={8}
            zoom={16}
            defaultCenter={{
                lat: 40.458631,
                lng: -3.694984
            }}
            center={{
                lat: center.lat,
                lng: center.lng
            }}
            defaultOptions={{
                styles: mapStyles,
                disableDefaultUI: true
            }}
        >
            {guardianes.guardianes.map(guardian => (
                <Marker
                    key={guardian.id}
                    position={{
                        lat: guardian.coordenadas.lat,
                        lng: guardian.coordenadas.lng
                    }}
                    draggable={false}
                    icon={{
                        url:
                            guardian.tipo === "particular"
                                ? home_marker
                                : shop_marker,
                        scaledSize: new window.google.maps.Size(75, 75)
                    }}
                    animation={2}
                    clickable={true}
                    onClick={() => {
                        handleClick(guardian);
                    }}
                />
            ))}

            {selectedIcon && <GuardianPopUp guardian={selectedIcon} />}

            <Marker
                position={{ lat: 40.458601, lng: -3.694438 }}
                draggable={false}
                id="userLocationMarker"
                icon={{
                    url: map_marker_green,
                    scaledSize: new window.google.maps.Size(75, 75)
                }}
            />
        </GoogleMap>
    );
};

//------DESDE AQUI HASTA EL FINAL ES LO QUE PINTA EL MAPA PER SE CON LOS DATOS DE ARRIBA------//

const WrappedMap = withScriptjs(withGoogleMap(PintarMapaFuncional));

const MapFuncional = ({ newCenter }) => {
    return (
        <div style={{ width: "100vw", height: "100vh" }}>
            <WrappedMap
                googleMapURL={`https://maps.googleapis.com/maps/api/js?key=AIzaSyCMq55J4NHJ1dzOtMOgYKVgYWN3g4WD4A4&v=3.exp&libraries=geometry,drawing,places`}
                loadingElement={<div style={{ height: "100%" }} />}
                containerElement={<div style={{ height: "100%" }} />}
                mapElement={<div style={{ height: "100%" }} />}
            />
        </div>
    );
};

export default MapFuncional;
