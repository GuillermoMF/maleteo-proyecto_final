import React, { useState, useEffect } from "react";
import PlacesAutocomplete, {
    geocodeByAddress,
    getLatLng
} from "react-places-autocomplete";
import { MapContainerFuncional } from '../MapContainerFuncional/MapContainerFuncional';


const Buscador = (props) => {

    const [address, setAddress] = useState("");
    const [coordinates, setCoordinates] = useState({
        lat: null, lng: null
    });
    const [placeholder, setStatePlaceholder] = useState(null);

    const handleSelect = async (value) => {
        const result = await geocodeByAddress(value);
        const searchedLatLng = await getLatLng(result[0]);
        setAddress(value);
        setCoordinates({ lat: searchedLatLng.lat, lng: searchedLatLng.lng });
        props.setStatePlaceholder(value);
    }

    console.log("Las coordenadas de ", address, " son: ", coordinates);

    return <div>
        <PlacesAutocomplete
            value={address}
            onChange={setAddress}
            onSelect={handleSelect}>

            {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                <div>
                    <input {...getInputProps({ placeholder: "¿Donde te encuentras? Madrid, Barcelona..." })} />

                    <div>
                        {loading ? <div>...loading</div> : null}

                        {suggestions.map(suggestion => {
                            const style = {
                                backgroundColor: suggestion.active ? "#ffa34e" : "#fff"
                            };

                            return (
                                <div
                                    {...getSuggestionItemProps(suggestion, { style })}>
                                    {suggestion.description}
                                </div>
                            );
                        })}
                    </div>
                </div>
            )}
        </PlacesAutocomplete>
        <MapContainerFuncional coordinates={coordinates} />
    </div>
}

export default Buscador;