import React, { Fragment, useState, useContext } from 'react'
import { Redirect } from 'react-router-dom'
import { Direcciones } from '../../util/Recursos'
import Atras from '../Atras/Atras'
import Login from './assets/componentes/Login/Login'
import Registro from './assets/componentes/Registro/Registro'

import './assets/styles/Conexion.css'


const Conexion = () => {
    const routerDir = useContext(Direcciones)

    const [visible, setVisible] = useState(true)
    const [rediret, setRedirect] = useState(false)

    const conectar = (exito) => {
        exito ? alert('Usuario registrado') : alert('Usuario ya existe')

        setVisible(<Login />)
    }
    const redireccionar = () => {
        if (rediret)
            return <Redirect to={routerDir.inicio} />
    }

    return (
        <Fragment>
            {redireccionar()}
            <div className="container-fluid ">
                <div className="row">
                    <div className="btnAtras">
                        <Atras volver={() => setRedirect(true)} />
                    </div>
                </div>
                <div className="eleccion">
                    <p onClick={() => { setVisible(true) }} name="login" className="menuEleccion textLarge">
                        Iniciar sesión
            </p>
                    <p onClick={() => { setVisible(false) }} name="registro" className="menuEleccion textLarge">
                        Regístrate
            </p>
                </div>
                {visible && <Login />}
                {!visible && <Registro conectar={conectar} />}
            </div>
        </Fragment>
    )
}

export default Conexion