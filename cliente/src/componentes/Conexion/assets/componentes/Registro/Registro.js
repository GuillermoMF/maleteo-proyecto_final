import React, { Fragment, useState, useContext } from 'react'
import { Direcciones, dirHotsAPI } from '../../../../../util/Recursos'
import RegistroAutomatico from '../RegistroAutomatico/RegistroAutomatico';
import Boton from '../BotonLog/BotonLog'
import './assets/styles/Registro.css';
import ImgOjo from './assets/images/eye.svg';

const passStyle = {
    display: 'flex'
}
const eyeStyle = {
    width: '20px',
}
const boxStyle = {
    border: '0px',
    borderBottom: '1px solid gray'
}
const myStyleBoton = {
    border: '1px solid gray',
    color: 'white',
    background: '#FF8300',
    width: '70%',
    margin: 'auto',
    marginTop: '10%',
    borderRadius: '4px',
    fontWeigth: 'bold',
    padding: '2%',
}

const Registro = (props) => {
    const host = useContext(dirHotsAPI)
    const routerDir = useContext(Direcciones)

    const [fecha, setFecha] = useState('')
    const [form, setForm] = useState(
        {
            body: {
                email: '',
                pass: '',
                name: '',
                surname: ''
            },
            boton: "Registrarse",
            url: host.api + routerDir.inicio + routerDir.register
        }
    )
    const recursos = {
        mostrarPass: false,
        titulo: "Únete a Maleteo y Disfruta de sus ventajas",
        aclaracion: "Para registrarte tendrás que ser mayor de edad. Los usuarios no veran tu fecha de cumpleaños",
        checkText: "Quiero recibir consejos sobre como gestionar mi equipaje, ofertas, novedades y otros correos electrónicos en Maleteo"
    }

    const resolver = (res) => {

        if (res.registrado)
            props.conectar(res.exito)
        else
            alert('Datos incorrectos')
    }

    const mostrarPass = () => {
        let pass = document.getElementById("rPass")

        if (!recursos.mostrarPass) {
            recursos.mostrarPass = true
            pass.type = 'text'
        } else {
            recursos.mostrarPass = false
            pass.type = 'password'
        }
    }

    const comprobar = () => {

        let nacimiento = new Date(document.getElementById("nacimiento").value)
        let email = document.getElementById("email").value
        let minimo = new Date()
        minimo.setFullYear(minimo.getFullYear() - 18)

        if (compEmail(email))
            if (nacimiento > minimo)
                alert(recursos.aclaracion)
            else
                if (document.getElementById("checkReg").checked && nacimiento < minimo) {
                    let regBoton = document.getElementById("regBoton")
                    regBoton.className = 'mostrar'
                }
    }
    const compEmail = (email) => {
        let aux = email.split('@')

        if (aux.length === 2 && aux[1] !== '') {
            aux = aux[1].split('.')
            if (aux.length === 2 && aux[1] !== '')
                return true
            else {
                alert('El email no es correcto')
                return false
            }
        }
        else {
            alert('El email no es correcto')
            return false
        }
    }

    return <Fragment>
        <p className="">
            {recursos.titulo}
        </p>
        <RegistroAutomatico />
        <p className="">
            O utiliza tu correo electrónico
            </p>
        <form>
            <div className="">
                <label className="">
                    Dirección de correo electrónico
                    </label>
                <input type="email" value={form.body.email} onBlur={comprobar} id='email'
                    onChange={(e) => setForm({ ...form, body: { ...form.body, email: e.target.value } })}
                    className="formulario-comp cajasForm"
                    style={boxStyle} />
            </div>
            <div className="">
                <label className="">
                    Nombre
                                </label>
                <input type="text" value={form.body.name}
                    onChange={(e) => setForm({ ...form, body: { ...form.body, name: e.target.value } })}
                    className="formulario-comp cajasForm"
                    style={boxStyle} />
            </div>
            <div className="">
                <label className="">
                    Apellido
                                </label>
                <input type="text" value={form.body.surname}
                    onChange={(e) => setForm({ ...form, body: { ...form.body, surname: e.target.value } })}
                    className="formulario-comp cajasForm"
                    style={boxStyle} />
            </div>
            <div className="">
                <label className="">
                    Contraseña
                    </label>
                <div style={passStyle}>
                    <input type="password" id="rPass" value={form.body.pass}
                        onChange={(e) => setForm({ ...form, body: { ...form.body, pass: e.target.value } })}
                        className="formulario-comp cajasForm"
                        style={boxStyle} />
                    <img onClick={mostrarPass} src={ImgOjo} alt="Ver" style={eyeStyle} />
                </div>
            </div>
            <div className="">
                <label className="">
                    Fecha de nacimiento
                                </label>
                <input type="date" value={fecha} onBlur={comprobar} id="nacimiento"
                    onChange={(e) => setFecha(e.target.value)}
                    className="formulario-comp cajasForm"
                    style={boxStyle} />
                <label className="textSmall">
                    {recursos.aclaracion}
                </label>
            </div>
            <div className="check">
                <input type="checkbox" id="checkReg" name="privacidad" onChange={comprobar} className=""
                    style={boxStyle} />
                <label className="" >
                    {recursos.checkText}
                </label>
            </div>
            <div id="regBoton" className="oculto">
                <div style={myStyleBoton}>
                    <Boton {...form} resolver={resolver} />
                </div>
            </div>
        </form>
    </Fragment >
}

export default Registro