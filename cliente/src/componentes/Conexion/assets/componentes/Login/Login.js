import React, { Fragment, useState, useContext } from 'react'
import { Redirect } from 'react-router-dom'
import { Direcciones, dirHotsAPI } from '../../../../../util/Recursos'
import RegistroAutomatico from '../RegistroAutomatico/RegistroAutomatico'
import Boton from '../BotonLog/BotonLog'
import './assets/styles/Login.css'

const myStyleBoton = {
    border: '1px solid gray',
    color: 'white',
    background: '#FF8300',
    width: '70%',
    margin: 'auto',
    marginTop: '10%',
    borderRadius: '4px',
    fontWeigth: 'bold',
    padding: '2%',
}
const myStyle = {
    fontWeight: 'bold',
}
const boxStyle = {
    border: '0px',
    borderBottom: '1px solid gray'
}
const Login = () => {
    const host = useContext(dirHotsAPI)
    const routerDir = useContext(Direcciones)

    const [rediret, setRedirect] = useState(false)
    const [form, setForm] = useState(
        {
            body: {
                email: "",
                pass: ""
            },
            boton: "Inicia sesión",
            url: host.api + routerDir.inicio + routerDir.login
        }
    )

    const recursos = {
        titulo: "Inicia sesión ahora"
    }

    const resolver = (res) => {

        if (res.exist)
            if (res.login)
                setRedirect(true)
            else {
                alert(res.respuesta)
                setForm({ ...form, body: { ...form.body, pass: '' } })
            }
        else
            alert(res.respuesta)
    }

    const redireccionar = () => {
        if (rediret)
            return <Redirect to={routerDir.inicio + routerDir.home} />
    }

    return <Fragment>
        {redireccionar()}
        <div className="row">
            <div className="">
                <p className="mobileHeading-2" style={myStyle}>
                    {recursos.titulo}
                </p>
            </div>
        </div>
        <RegistroAutomatico />
        <div className="row">
            <p className="">
                O utiliza tu correo electrónico
                </p>
        </div>
        <div className="row">
            <form >
                <div className="">
                    <label className="formulario-comp cajasForm">
                        Dirección de correo electrónico
                            </label>
                    <input type="email" value={form.body.email}
                        onChange={(e) => setForm({ ...form, body: { ...form.body, email: e.target.value } })}
                        className="formulario-comp cajasForm"
                        style={boxStyle} />
                </div>
                <div className="">
                    <label className="formulario-comp cajasForm">
                        Contraseña
                            </label>
                    <input type="password" value={form.body.pass}
                        onChange={(e) => setForm({ ...form, body: { ...form.body, pass: e.target.value } })}
                        className="formulario-comp cajasForm"
                        style={boxStyle} />
                </div>
                <div className="formulario-comp cajasForm" style={myStyleBoton} >
                    <Boton {...form} resolver={resolver} />
                </div>
            </form>
        </div>
    </Fragment>
}
export default Login