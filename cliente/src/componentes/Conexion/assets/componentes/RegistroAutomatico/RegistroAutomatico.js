import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { DirExterno } from '../../../../../util/Recursos'
import './assets/styles/RegistroAutomatico.css';
import facebook from './assets/images/vectorpaint.svg'
import google from './assets/images/search.png'

const fg = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: "space-between"
}

const fb = {
    display: 'flex',
    width: '40%',
    backgroundColor: '#4267b2',
    height: '30px'
}

const gl = {
    display: 'flex',
    width: '40%',
    height: '30px',
    border: '2px solid gray',
    paddingTop: '10px'
}


const fbi = {
    maxHeight: '75%',
    marginLeft: '4px',
    marginTop: '3px'
}

const face = {
    color: 'white',
    padding: '8px'
}
const GgleStyle = {
    maxHeight: '21px',
    paddingLeft: '1em',
}
// const GgleStyle2 = {
//     display: 'flex',
//     width: '50%',
//     marginLeft: '2%',
//     
// }
const pStyle = {
    display: 'inline-block',
    margin: '2%',
    paddingLeft: '13%',
    marginTop: '3%'
}

const RegistroAutomatico = () => {
    const exit = useContext(DirExterno)

    return (
        <div style={fg}>
            <Link to={exit.facebook} style={fb}>
                <img
                    src={facebook}
                    alt="Facebook"
                    style={fbi} />
                <span style={face}>
                    Facebook
                </span>
            </Link>
            <Link to={exit.facebook} style={gl}>
                <img
                    src={google}
                    alt="Google"
                    style={GgleStyle}
                    className="" />

                <p style={pStyle}>Google</p>
            </Link>
        </div>
    )
}

export default RegistroAutomatico