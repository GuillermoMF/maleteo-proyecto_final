import React from 'react'
import './assets/styles/BotonLog.css'

const myStyle = {
    margin: '4px',
    textAlign: 'center',
}

const Boton = (props) => {

    const datos = {
        method: 'post',
        body: JSON.stringify(props.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }

    const enviar = async () => {
        const res = await fetch(props.url, datos)
        const data = await res.json()
        props.resolver(data)
        // .then(res => res.json())
        // .then(res => props.resolver(res))
        // .catch(err => console.log(err))
    }

    return <div className="boton">
        <p onClick={enviar} className="textLarge" style={myStyle}>{props.boton}</p>
    </div>
}

export default Boton
