import React from "react";
import StarRatingComponent from "react-star-rating-component";

const ratingNum = {
  marginLeft: "0.5em"
};

const EstrellitasCards = ({ guardian }) => {
  return (
    <div>
      {/*<p>{this.state.count}*/}
      <StarRatingComponent
        name={guardian.nombreApellido}
        starCount={5}
        value={guardian.puntuacion}

      //onStarClick={this.onStarClick.bind(this)}
      />
      <span style={ratingNum}>{guardian.puntuacion}</span>
      {/*{this.state.media}*/}
      {/*</p>*/}
    </div>
  );
};

export default EstrellitasCards;
