import React, { useState, useEffect } from "react";
import Buscador from "../PlacesAutocomplete/PlacesAutocomplete";
//import Geocode from "react-geocode";
//
//Geocode.setApiKey("AIzaSyCMq55J4NHJ1dzOtMOgYKVgYWN3g4WD4A4");
//Geocode.enableDebug();

function Geolocate() {
  const [geolocation, setGeolocation] = useState({
    lat: null,
    lng: null
  });

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(position => {
      const { latitude, longitude } = position.coords;
      setGeolocation({ lat: latitude, lng: longitude });
    });
  }, []);

  console.log("Tu posición geolocalizada es: ", geolocation);
  return (
    <div>
      <Buscador geolocation={geolocation} />
    </div>
  );
}

export default Geolocate;
