import React from 'react'

const b = {
    color: 'white'
}

const Boton = (props) => {


    const datos = {
        method: 'post',
        body: JSON.stringify(props.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }

    const enviar = async () => {
        const res = await fetch(props.url, datos)
        const data = await res.json()
        props.resolver(data)
        // .then(res => res.json())
        // .then(res => props.resolver(res))
        // .catch(err => console.log(err))
    }

    return <div className="boton">
        <p style={b}>Buscar</p>
        {/* <p onClick={enviar} className="textLarge" style={myStyle}>{props.boton}</p> */}
    </div>
}

export default Boton
