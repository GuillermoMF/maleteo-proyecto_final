import React, { Component, Fragment } from 'react';

import Popup from "reactjs-popup";
import Buscador from '../PlacesAutocomplete/PlacesAutocomplete';
import lupa from './assets/images/lupa.svg';


const palceholder1 = {
  zIndex: '10'
}

const spanlupa = {
  display: 'flex',
  alignItems: 'center'
}
const flex = {
  width: '100%',
  display: 'flex',
  justifyContent: 'flex-start',
  height: '38px',
  alignItems: 'center',
  background: 'transparent',
  border: 'none',
  boxShadow: '1px 1px 3px gray'
};

const imglup = {
  width: '20px',
  marginRight: '70%',

};
const grid = {
  width: '100%',
  height: '38px',
  marginBottom: '4px',
}


class ControlledPopup extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false, placeholder: '¿Donde te encuentras? Madrid, Barcelona...' };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.setStatePlaceholder = this.setStatePlaceholder.bind(this);
  }
  openModal() {
    this.setState({ open: true });
  }
  closeModal() {
    this.setState({ open: false });
  }
  setStatePlaceholder(placeholder) {
    this.setState({ placeholder });
  }

  render() {
    return (
      <Fragment>
        <div style={grid}>
          <button className="busqueda" onClick={this.openModal} style={flex}>
            <span style={spanlupa}>
              <div className="imagen">
                <img className="lupa" src={lupa} alt="lupa" style={imglup} />
              </div>
            </span>
            <span style={palceholder1}>
              {this.state.placeholder}
            </span>
          </button>
        </div>
        <Popup
          open={this.state.open}
          closeOnDocumentClick
          onClose={this.closeModal}
        >
          <div className="modal">
            <a className="close" onClick={this.closeModal}>
              &times;
            </a>
            <Buscador setStatePlaceholder={this.setStatePlaceholder}></Buscador>
          </div>
        </Popup>
      </Fragment>
    );
  }
}

export default ControlledPopup;