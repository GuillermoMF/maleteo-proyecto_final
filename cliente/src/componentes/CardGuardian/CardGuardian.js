import React, {Component} from 'react';
import linea from './assets/images/linea.svg';
import security from './assets/images/security.svg'
const CardBlogStyle={
    width:'75%',

}
const svg={
    height: '15%',
    width: '29%',
    marginTop:'auto',
    marginBottom:'auto',

}
const svgblog={
    display: 'flex',
}
const line={
    maxHeight:'2px',
    width:'100%'
} 
const color={
    color:'white'
}
const Bp={
    marginLeft: '30px'
}


class CardGuardianes extends Component{
render(){
    return(
        <div>
            <section style={CardBlogStyle}>
                <div className="svgblog" style={svgblog}>
                    <img className="security" src={security} alt="security" style={svg}/>
                    <div className="write" style={Bp}>
                        <h1 style={color}>GUARDIANES</h1>
                        <p style={color}>Todo lo que necesitas saber sobre los guardianes</p>
                    </div>
                </div>
                <div className="linea">    
                    <img src={linea} alt="linea" style={line}/>
                </div>
                <div className="Leermas">
                    <p style={color}>Leer más</p>
                </div>
            </section>
            </div>
    );
};
}

export default CardGuardianes;