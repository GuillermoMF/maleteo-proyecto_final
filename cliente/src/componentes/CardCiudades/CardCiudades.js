import React, {Component} from 'react';
import linea from './assets/images/linea.svg';
import city from './assets/images/city.svg'
const CardBlogStyle={
    width:'75%',
}
const svg={
    height: '15%',
    width: '29%',
    marginTop: '43px'
}
const svgblog={
    display: 'flex'
}
const line={
    maxHeight:'2px',
    width:'100%'
} 
const color={
    color:'white'
}
const Bp={
    marginLeft: '30px'
}



class CardCIudades extends Component{
render(){
    return(
        <div>
            <section style={CardBlogStyle}>
                <div className="svgblog" style={svgblog}>
                    <img className="city" src={city} alt="Blog" style={svg}/>
                    <div className="write" style={Bp}>
                        <h1 style={color}>CIUDADES</h1>
                        <p style={color}>Áticulos sobre ciudades</p>
                    </div>
                </div>
                <div className="linea">    
                    <img src={linea} alt="linea" style={line}/>
                </div>
                <div className="Leermas">
                    <p style={color}>Leer más</p>
                </div>
            </section>
            </div>
    );
};
}

export default CardCIudades;