import React, { Component } from 'react';

import StarRatingComponent from 'react-star-rating-component';

class Estrellitas extends Component {
  constructor() {
    super();

    this.state = {
      rating: 1,
      val: 0,
      count: 0,
      media: 0

    };
  }

  onStarClick(nextValue, prevValue, name) {
    this.setState({ rating: nextValue });
    this.setState({ val: this.state.rating + this.state.val });
    this.setState({ count: this.state.count + 1 });
    this.setState({ media: (this.state.val / this.state.count).toFixed(1) });

    // eslint-disable-next-line use-isnan
    if ((this.state.media) === NaN) {

      this.setState({ media: 0 });
    }
  }

  render() {
    return (
      <div>
        <p>
          {this.state.count}
          <StarRatingComponent name="rate1" starCount={5} value={this.state.rating} onStarClick={this.onStarClick.bind(this)} />
          {this.state.media}
        </p>
      </div>
    );
  };
}
export default Estrellitas;
