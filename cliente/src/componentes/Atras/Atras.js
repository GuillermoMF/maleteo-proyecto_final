import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { Direcciones } from '../../util/Recursos'
import FlechaAtras from './assets/images/flecha-hacia-la-izquierda.png'

const Atras = () => {
    const routerDir = useContext(Direcciones)
    console.log(routerDir.ultima)
    return (
        <Link to={routerDir.ultima}>
            <img src={FlechaAtras} alt="Atras" className="" />
        </Link>)

}


export default Atras