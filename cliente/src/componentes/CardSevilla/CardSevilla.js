import React, {Component} from 'react'
import sevilla from './assets/images/sevilla.jpg'
import Estrellitas from '../Estrellitas/Estrellitas.js'

const foto={
    width:'100%',
    borderRadius:'4px'
}
const myStyle = {
    color:'gray',
}


class CardSevilla extends Component{
render(){
    return(

        <div style={myStyle}>
            <img src={sevilla} style={foto} alt="sevilla"/>
            <p>
                <strong>
                    La Giralda presume orgullosa
                </strong>
            </p>
            <p>
                EL arte, el flamenco, la gracia, los 
                monumentos. Cultura y tradición en una 
                ciudad única.
            </p>
            <Estrellitas></Estrellitas>
        </div>
        );
    };

}

export default CardSevilla; 
