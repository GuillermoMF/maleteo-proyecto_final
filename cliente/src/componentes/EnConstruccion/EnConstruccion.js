import React, { Fragment, useState, useContext } from 'react'
import { Redirect } from 'react-router-dom'
import { Direcciones } from '../../util/Recursos'
import Atras from '../Atras/Atras'
import './assets/styles/EnConstruccion.css'
import en_desarrollo from './assets/images/en-desarrollo.jpg'

const myStyle={
    maxWidth:'100%',
    marginTop:'25%',

}
const myStyle2={
    maxWidth:'inherit',
}

const pStyle={
    width:'100%',
    textAlign:'center',
    color:'gray',
}
const EnConstruccion = () => {
    const routerDir = useContext(Direcciones)

    const [rediret, setRedirect] = useState(false)

    const redireccionar = () => {
        if (rediret)
            return <Redirect to={routerDir.inicio + routerDir.home} />
    }

    return (
        <Fragment>
            {redireccionar()}
            <div className="container-fluid ">
                <div className="row">
                    <div className="btnAtras">
                        <Atras volver={() => setRedirect(true)} />
                    </div>
                </div>
                <div className="eleccion" >
                    <div className="mobileHeading-2"  style={myStyle}>
                        <img src={en_desarrollo} alt="Construccion" className="" style={myStyle2} />
                          <p style={pStyle}>
                            Esta página esta en desarrollo...
                          </p>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default EnConstruccion