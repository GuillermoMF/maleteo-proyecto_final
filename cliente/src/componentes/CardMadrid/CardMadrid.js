import React, {Component} from 'react';
import madrid from './assets/images/madrid.jpg'
import Estrellitas from '../Estrellitas/Estrellitas.js'

const foto={
    width:'100%',
    borderRadius:'4px'
}
const myStyle = {
    color:'gray',
}


class CardMadrid extends Component{
render(){
    return(

        <div style={myStyle}>
            <img src={madrid} style={foto} alt="madrid"/>
            <p>
                <strong>
                    Un pedazito de cielo en la capital
                </strong>
            </p>
            <p>
                Podrás disfrutal de la capital. Una ciudad
                llena de cultura y que combina la parte cosmopolitan
                de la ciudad con la historia
            </p>
            <Estrellitas></Estrellitas>
        </div>
        );
    };

}

export default CardMadrid;
