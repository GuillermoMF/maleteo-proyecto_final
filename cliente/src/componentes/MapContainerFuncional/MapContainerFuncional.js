import React, { useState, useEffect } from 'react';
import Geocode from 'react-geocode';
import PintarMapaFuncional from '../PintarMapaFuncional/PintarMapaFuncional';

Geocode.setApiKey("AIzaSyCMq55J4NHJ1dzOtMOgYKVgYWN3g4WD4A4");
Geocode.enableDebug();

export const MapContainerFuncional = ({ coordinates, geolocation }) => {
  console.log("Las coordenadas que llegan son: ", coordinates);

  const [newCenter, setNewCenter] = useState({ lat: null, lng: null });
  //const [loading, setLoading] = useState (true);
  //const [zoom, setZoom] = useState (8);

  useEffect(() => {
    setNewCenter(coordinates);
    //setZoom(17);
  }, [coordinates]);
  console.log("Las coordenadas actuales son: ", newCenter);
  //console.log("El loading actual es : ", loading);
  //console.log("El zoom actual es: ", zoom);

  return (
    <div>
      <PintarMapaFuncional newCenter={newCenter} />
    </div>
  );
};
