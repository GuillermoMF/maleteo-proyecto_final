import React from 'react'

export const dirHotsAPI = React.createContext({
    conectado: false,
    api: "http://localhost:9000"
})

export const Direcciones = React.createContext({
    inicio: '/',
    home: 'home',
    tarifas: 'tarifas',
    login: 'login',
    register: 'register',
    buscar: 'buscar',
    chat: 'chat',
    perfil: 'perfil',
    ultima: ''
})

export const DirExterno = React.createContext({
    facebook: "https://es-es.facebook.com",
    google: "https://www.google.com/"
})