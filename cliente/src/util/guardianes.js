export default {
    "guardianes": [{
        "id": 1,
        "tipo": "particular",
        "nombreApellido": "Ana Pérez Marín",
        "coordenadas": {
            "lat": 40.4170,
            "lng": -3.7045
        },

        "puntuacion": 5,
        "icono": "/markers/home-marker.svg",
        "iconoSelected": "/markers/home-marker-blue.svg",
        "descripcion": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis, arcu vel ultricies luctus, odio ipsum iaculis orci, ac porttitor est lorem at leo. Nam odio purus, sagittis non sapien sit amet, vulputate lacinia felis. Duis elementum diam faucibus, consectetur turpis sit amet, lobortis risus. Vivamus malesuada imperdiet lectus ut pulvinar. Mauris at volutpat elit, nec lacinia est."
    },

    {
        "id": 2,
        "tipo": "tienda",
        "nombreApellido": "Medias Pili",
        "coordenadas": {
            "lat": 40.4174,
            "lng": -3.7049
        },

        "puntuacion": 4,
        "icono": "/markers/shop-marker.svg",
        "iconoSelected": "/markers/shop-marker-blue.svg",
        "descripcion": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis, arcu vel ultricies luctus, odio ipsum iaculis orci, ac porttitor est lorem at leo. Nam odio purus, sagittis non sapien sit amet, vulputate lacinia felis. Duis elementum diam faucibus, consectetur turpis sit amet, lobortis risus. Vivamus malesuada imperdiet lectus ut pulvinar. Mauris at volutpat elit, nec lacinia est."
    },

    {
        "id": 3,
        "tipo": "particular",
        "nombreApellido": "Felix Martinez",
        "coordenadas": {
            "lat": 40.406580,
            "lng": -3.684527
        },

        "puntuacion": 1,
        "icono": "/markers/home-marker.svg",
        "iconoSelected": "/markers/home-marker-blue.svg",
        "descripcion": "Phasellus rutrum, sem a venenatis rutrum, dolor augue dignissim lorem, eget blandit quam urna non risus. Cras vitae condimentum erat, ut maximus diam."
    },

    {
        "id": 4,
        "tipo": "tienda",
        "nombreApellido": "Almacen Pontejos",
        "coordenadas": {
            "lat": 40.416120,
            "lng": -3.704200
        },

        "puntuacion": 4,
        "icono": "/markers/shop-marker.svg",
        "iconoSelected": "/markers/shop-marker-blue.svg",
        "descripcion": "Suspendisse tincidunt augue vel blandit lobortis. Maecenas rutrum feugiat erat eu ultrices. Duis id pharetra tortor. Donec augue nisi, sagittis id fermentum in, pellentesque sit amet sapien."
    },

    {
        "id": 5,
        "tipo": "particular",
        "nombreApellido": "Morgan Freeman",
        "coordenadas": {
            "lat": 40.461058,
            "lng": -3.695140
        },

        "puntuacion": 3,
        "icono": "/markers/home-marker.svg",
        "iconoSelected": "/markers/home-marker-blue.svg",
        "descripcion": "Phasellus luctus metus quis malesuada posuere. Aliquam lacus mauris, suscipit ut lectus at, rhoncus finibus lorem."
    },

    {
        "id": 6,
        "tipo": "tienda",
        "nombreApellido": "La Papita",
        "coordenadas": {
            "lat": 40.459907,
            "lng": -3.699045
        },

        "puntuacion": 1,
        "icono": "/markers/shop-marker.svg",
        "iconoSelected": "/markers/shop-marker-blue.svg",
        "descripcion": "Proin fermentum dignissim nulla vitae iaculis. Fusce rhoncus leo eu massa elementum egestas."
    },

    {
        "id": 7,
        "tipo": "tienda",
        "nombreApellido": "Restaurante Ondiñas do Mendo",
        "coordenadas": {
            "lat": 40.456870,
            "lng": -3.704948
        },

        "puntuacion": 3,
        "icono": "/markers/shop-marker.svg",
        "iconoSelected": "/markers/shop-marker-blue.svg",
        "descripcion": "Moral selfish superiority god prejudice chaos passion christian derive insofar. Derive dead reason ascetic gains dead free love intentions truth."
    },

    {
        "id": 8,
        "tipo": "particular",
        "nombreApellido": "Chiquito de la calzada",
        "coordenadas": {
            "lat": 40.459205,
            "lng": -3.682758
        },

        "puntuacion": 5,
        "icono": "/markers/home-marker.svg",
        "iconoSelected": "/markers/home-marker-blue.svg",
        "descripcion": "Lorem fistrum por la gloria de mi madre hasta luego Lucas hasta luego Lucas jarl. Ese que llega sexuarl de la pradera de la pradera al ataquerl llevame al sircoo ese que llega la caidita"
    },

    {
        "id": 9,
        "tipo": "tienda",
        "nombreApellido": "Moe",
        "coordenadas": {
            "lat": 40.458499,
            "lng": -3.682699
        },

        "puntuacion": 2,
        "icono": "/markers/shop-marker.svg",
        "iconoSelected": "/markers/shop-marker-blue.svg",
        "descripcion": "Ese pedazo de por la gloria de mi madre a peich no puedor apetecan pecador condemor."
    },

    {
        "id": 10,
        "tipo": "particular",
        "nombreApellido": "Florentino Pérez",
        "coordenadas": {
            "lat": 40.441297,
            "lng": -3.682759
        },

        "puntuacion": 4,
        "icono": "/markers/home-marker.svg",
        "iconoSelected": "/markers/home-marker-blue.svg",
        "descripcion": "Pecador diodeno diodeno la caidita a peich de la pradera. Benemeritaar amatomaa me cago en tus muelas caballo blanco caballo negroorl. Pupita mamaar a gramenawer torpedo por la gloria de mi madre."
    },

    {
        "id": 11,
        "tipo": "tienda",
        "nombreApellido": "Spa Balneario Madrid Sol y Agua",
        "coordenadas": {
            "lat": 40.457820,
            "lng": -3.697563
        },

        "puntuacion": 2,
        "icono": "/markers/shop-marker.svg",
        "iconoSelected": "/markers/shop-marker-blue.svg",
        "descripcion": "De la pradera al ataquerl diodeno a gramenawer pecador diodeno jarl quietooor diodenoo papaar papaar. Al ataquerl sexuarl sexuarl me cago en tus muelas."
    },

    {
        "id": 12,
        "tipo": "particular",
        "nombreApellido": "Julián Muñoz",
        "coordenadas": {
            "lat": 40.458689,
            "lng": -3.698496
        },

        "puntuacion": 1,
        "icono": "/markers/home-marker.svg",
        "iconoSelected": "/markers/home-marker-blue.svg",
        "descripcion": "Tenia una habitación pequeña, con una cama muy fría y el baño dentro. Encima tenía barrotes"
    },

    {
        "id": 13,
        "tipo": "tienda",
        "nombreApellido": "Kube Madrid",
        "coordenadas": {
            "lat": 40.459828,
            "lng": -3.691951
        },

        "puntuacion": 2,
        "icono": "/markers/shop-marker.svg",
        "iconoSelected": "/markers/shop-marker-blue.svg",
        "descripcion": "Local excesivamente frío. Se me congelaron los gallumbos y no podía ponermelos después."
    },

    {
        "id": 14,
        "tipo": "particular",
        "nombreApellido": "Jesús Gil",
        "coordenadas": {
            "lat": 40.463403,
            "lng": -3.688561
        },

        "puntuacion": 3,
        "icono": "/markers/home-marker.svg",
        "iconoSelected": "/markers/home-marker-blue.svg",
        "descripcion": "Llevame al sircoo ahorarr torpedo apetecan te va a hasé pupitaa ahorarr te voy a borrar el cerito mamaar. Benemeritaar pecador no te digo trigo por no llamarte Rodrigor papaar papaar va usté muy cargadoo papaar papaar tiene musho peligro te va a hasé pupitaa qué dise usteer."
    },

    {
        "id": 15,
        "tipo": "tienda",
        "nombreApellido": "Knight 'n' Squire (La Casa de las Hamburguesas)",
        "coordenadas": {
            "lat": 40.463342,
            "lng": -3.686748
        },

        "puntuacion": 5,
        "icono": "/markers/shop-marker.svg",
        "iconoSelected": "/markers/shop-marker-blue.svg",
        "descripcion": "Mamaar por la gloria de mi madre sexuarl tiene musho peligro. A gramenawer llevame al sircoo no puedor diodeno a gramenawer diodeno ahorarr al ataquerl al ataquerl torpedo pecador."
    },

    {
        "id": 16,
        "tipo": "particular",
        "nombreApellido": "Mariano Rajoy",
        "coordenadas": {
            "lat": 40.461346,
            "lng": -3.685278
        },

        "puntuacion": 5,
        "icono": "/markers/home-marker.svg",
        "iconoSelected": "/markers/home-marker-blue.svg",
        "descripcion": "Four dollar toast 3 wolf moon quis, listicle seitan sunt kitsch taxidermy snackwave meggings. Sed pok pok crucifix kombucha 90's non. "
    },

    {
        "id": 17,
        "tipo": "particular",
        "nombreApellido": "Santana Art Gallery",
        "coordenadas": {
            "lat": 40.461633,
            "lng": -3.689220
        },

        "puntuacion": 2,
        "icono": "/markers/shop-marker.svg",
        "iconoSelected": "/markers/shop-marker-blue.svg",
        "descripcion": "Fearful grandeur gains derive self ubermensch depths. Gains selfish gains eternal-return hope chaos hatred ocean."
    },

    {
        "id": 18,
        "tipo": "particular",
        "nombreApellido": "Paco Porras",
        "coordenadas": {
            "lat": 40.456225,
            "lng": -3.691924
        },

        "puntuacion": 1,
        "icono": "/markers/home-marker.svg",
        "iconoSelected": "/markers/home-marker-blue.svg",
        "descripcion": "Diodeno de la pradera qué dise usteer mamaar. Benemeritaar ese pedazo de no te digo trigo por no llamarte Rodrigor pupita no puedor."
    },

    {
        "id": 19,
        "tipo": "tienda",
        "nombreApellido": "Sauna-Bar",
        "coordenadas": {
            "lat": 40.458893,
            "lng": -3.694997
        },

        "puntuacion": 5,
        "icono": "/markers/shop-marker.svg",
        "iconoSelected": "/markers/shop-marker-blue.svg",
        "descripcion": "A peich ese hombree sexuarl te va a hasé pupitaa la caidita fistro ahorarr jarl tiene musho peligro se calle ustée papaar papaar."
    },

    {
        "id": 20,
        "tipo": "particular",
        "nombreApellido": "La Veneno",
        "coordenadas": {
            "lat": 40.458656,
            "lng": -3.697175
        },

        "puntuacion": 1,
        "icono": "/markers/home-marker.svg",
        "iconoSelected": "/markers/home-marker-blue.svg",
        "descripcion": "La caidita tiene musho peligro amatomaa me cago en tus muelas benemeritaar te va a hasé pupitaa a peich papaar papaar fistro papaar papaar."
    },

    {
        "id": 21,
        "tipo": "tienda",
        "nombreApellido": "Ibero China",
        "coordenadas": {
            "lat": 40.461178,
            "lng": -3.695630
        },

        "puntuacion": 5,
        "icono": "/markers/shop-marker.svg",
        "iconoSelected": "/markers/shop-marker-blue.svg",
        "descripcion": "Pecador llevame al sircoo diodeno quietooor de la pradera a peich te voy a borrar el cerito hasta luego Lucas benemeritaar. "
    },

    {
        "id": 22,
        "tipo": "particular",
        "nombreApellido": "Donal Trump",
        "coordenadas": {
            "lat": 40.468573,
            "lng": -3.685373
        },

        "puntuacion": 5,
        "icono": "/markers/home-marker.svg",
        "iconoSelected": "/markers/home-marker-blue.svg",
        "descripcion": "Por la gloria de mi madre al ataquerl llevame al sircoo te va a hasé pupitaa torpedo amatomaa a wan apetecan. Amatomaa jarl ese que llega ese hombree."
    },

    {
        "id": 23,
        "tipo": "tienda",
        "nombreApellido": "Diana Montoya Estética",
        "coordenadas": {
            "lat": 40.464590,
            "lng": -3.681678
        },

        "puntuacion": 2,
        "icono": "/markers/shop-marker.svg",
        "iconoSelected": "/markers/shop-marker-blue.svg",
        "descripcion": "Se calle ustée diodenoo papaar papaar de la pradera apetecan. No te digo trigo por no llamarte Rodrigor mamaar está la cosa muy malar por la gloria de mi madre pecador qué dise usteer fistro."
    },

    {
        "id": 24,
        "tipo": "particular",
        "nombreApellido": "Chenoa",
        "coordenadas": {
            "lat": 40.460248,
            "lng": -3.680073
        },

        "puntuacion": 5,
        "icono": "/markers/home-marker.svg",
        "iconoSelected": "/markers/home-marker-blue.svg",
        "descripcion": "Se calle ustée diodenoo papaar papaar de la pradera apetecan. No te digo trigo por no llamarte Rodrigor mamaar está la cosa muy malar por la gloria de mi madre pecador qué dise usteer fistro."
    }
    ]
}